package ee.bcs.valiit.Excercises5.Ex2;

public class Countries extends CountryInfo {
    public static void main(String[] args) {
        CountryInfo country1 = new CountryInfo();
        country1.setCountry("Estonia");
        country1.setCapital("Tallinn");
        country1.setPrimeMinister("Jüri Ratas");
        country1.setLanguages(new String[]{"Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"});

        CountryInfo country2 = new CountryInfo();
        country2.setCountry("Latvia");
        country2.setCapital("Riga");
        country2.setPrimeMinister("Māris Kučinskis");
        country2.setLanguages(new String[]{"Latvian", "Russian", "Ukrainian", "Belarusian"});

        CountryInfo country3 = new CountryInfo();
        country3.setCountry("Lithuania");
        country3.setCapital("Vilnius");
        country3.setPrimeMinister("Saulius Skvernelis");
        country3.setLanguages(new String[]{"Lithuanian", "Russian", "Ukrainian", "Belarusian", "Polish"});

        CountryInfo country4 = new CountryInfo();
        country4.setCountry("Finland");
        country4.setCapital("Helsinki");
        country4.setPrimeMinister("Juha Sipilä");
        country4.setLanguages(new String[]{"Finnish", "Swedish", "Russian"});

        CountryInfo country5 = new CountryInfo();
        country5.setCountry("Sweden");
        country5.setCapital("Stockholm");
        country5.setPrimeMinister("Stefan Löfven");
        country5.setLanguages(new String[]{"Swedish", "Finnish", "Meänkieli", "Sami"});

        CountryInfo country6 = new CountryInfo();
        country6.setCountry("Spain");
        country6.setCapital("Madrid");
        country6.setPrimeMinister("Mariano Rajoy");
        country6.setLanguages(new String[]{"Spanish", "Basque", "Catalan"});

        CountryInfo country7 = new CountryInfo();
        country7.setCountry("Croatia");
        country7.setCapital("Zagreb");
        country7.setPrimeMinister("Andrej Plenković");
        country7.setLanguages(new String[]{"Croatian", "Czech", "Hungarian", "Italian"});

        CountryInfo country8 = new CountryInfo();
        country8.setCountry("Denmark");
        country8.setCapital("Copenhagen");
        country8.setPrimeMinister("Lars Løkke Rasmussen");
        country8.setLanguages(new String[]{"Danish", "Faroese", "German", "Swedish"});

        CountryInfo country9 = new CountryInfo();
        country9.setCountry("France");
        country9.setCapital("Paris");
        country9.setPrimeMinister("Édouard Philippe");
        country9.setLanguages(new String[]{"French", "English", "Spanish", "German"});

        CountryInfo country10 = new CountryInfo();
        country10.setCountry("Norway");
        country10.setCapital("Oslo");
        country10.setPrimeMinister("Erna Solberg");
        country10.setLanguages(new String[]{"Norwegian", "Sami", "English"});

        System.out.println(country1);
        System.out.println(country2);
        System.out.println(country3);
        System.out.println(country4);
        System.out.println(country5);
        System.out.println(country6);
        System.out.println(country7);
        System.out.println(country8);
        System.out.println(country9);
        System.out.println(country10);


    }
}