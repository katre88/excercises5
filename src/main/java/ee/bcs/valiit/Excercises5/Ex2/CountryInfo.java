package ee.bcs.valiit.Excercises5.Ex2;

import lombok.Data;

@Data
public class CountryInfo {
    protected String country;
    protected String capital;
    protected String primeMinister;
    protected String[] languages;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(country)
                .append(": ");
        for (String language : languages) {
            builder.append("\n \t" + language);

        }
        return builder.toString();

    }

}
