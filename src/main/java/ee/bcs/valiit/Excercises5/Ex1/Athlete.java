package ee.bcs.valiit.Excercises5.Ex1;

import lombok.Data;

@Data
//enam pole vaja getter-setter panna, määratleb Data klassi
public abstract class Athlete {
    protected String name;
    protected String lastName;
    protected int age;
    protected Gender gender;
    protected double height;
    protected double weight;

    public Athlete() {}

    public Athlete (String name, String lastName, int age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    public void perform() { }

    @Override
    public String toString() {
        StringBuilder athlete = new StringBuilder(name)
                .append(" ")
                .append(lastName)
                .append(", ")
                .append(age)
                .append(" years, ")
                .append(gender)
                .append(", ")
                .append(height)
                .append(" m, ")
                .append(weight)
                .append("kg");
        return athlete.toString();
    }

}
