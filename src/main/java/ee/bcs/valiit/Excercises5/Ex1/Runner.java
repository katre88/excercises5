package ee.bcs.valiit.Excercises5.Ex1;

public class Runner extends Athlete {

    public Runner() {
        super();
    }
    public Runner(String name, String lastName, int age) {
        super(name, lastName, age);
    }

    @Override
    public void perform() {
        System.out.println("Running");
    }
}
