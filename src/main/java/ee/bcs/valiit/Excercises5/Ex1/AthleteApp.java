package ee.bcs.valiit.Excercises5.Ex1;

public class AthleteApp {

    public static void main(String[] args) {
        Runner runner1 = new Runner();
        runner1.setName("Jüri");
        runner1.setLastName("Tamm");
        runner1.setAge(25);
        runner1.setGender(Gender.MALE);
        runner1.setHeight(1.76);
        runner1.setWeight(63);
        Runner runner2 = new Runner();
        runner2.setName("Kati");
        runner2.setLastName("Ploom");
        runner2.setAge(21);
        runner2.setGender(Gender.FEMALE);
        runner2.setHeight(1.65);
        runner2.setWeight(52);
        Runner runner3 = new Runner();
        runner3.setName("Mart");
        runner3.setLastName("Kirss");
        runner3.setAge(35);
        runner3.setGender(Gender.MALE);
        runner3.setHeight(1.85);
        runner3.setWeight(72);
        Skydiver skydiver1 = new Skydiver();
        skydiver1.setName("Linda");
        skydiver1.setLastName("Kask");
        skydiver1.setAge(25);
        skydiver1.setGender(Gender.FEMALE);
        skydiver1.setHeight(1.62);
        Skydiver skydiver2 = new Skydiver();
        skydiver2.setName("Mari");
        skydiver2.setLastName("Pihl");
        skydiver2.setAge(30);
        skydiver2.setGender(Gender.FEMALE);
        skydiver2.setHeight(1.67);
        skydiver2.setWeight(70);
        Skydiver skydiver3 = new Skydiver();
        skydiver3.setName("Toomas");
        skydiver3.setLastName("Lepp");
        skydiver3.setAge(25);
        skydiver3.setGender(Gender.MALE);
        skydiver3.setHeight(1.82);
        skydiver3.setWeight(89);
        Runner runner4 = new Runner("Priit", "Raud", 41);
        runner4.setGender(Gender.MALE);
        runner4.setHeight(1.77);
        runner4.setWeight(61);

        System.out.print(runner1 + " ");
        runner1.perform();
        System.out.print(skydiver1 + " ");
        skydiver1.perform();
        System.out.print(runner2 + " ");
        runner2.perform();
        System.out.print(skydiver2 + " ");
        skydiver2.perform();
        System.out.print(runner3 + " ");
        runner3.perform();
        System.out.print(skydiver3 + " ");
        skydiver3.perform();
        System.out.print(runner4 + " ");
        runner4.perform();


    }
}
