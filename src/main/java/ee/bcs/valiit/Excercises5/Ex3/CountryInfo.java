package ee.bcs.valiit.Excercises5.Ex3;

import lombok.Data;

@Data
public class CountryInfo {
    protected String country;
    protected String capital;
    protected String primeMinister;
    protected String[] languages;

    public CountryInfo (String country, String capital, String primeMinister, String[] languages) {
        this.country = country;
        this.capital = capital;
        this.primeMinister = primeMinister;
        this.languages = languages;
    }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(country)
                .append(": ");
        for (String language : languages) {
            builder.append("\n \t" + language);

        }
        return builder.toString();

    }

}