package ee.bcs.valiit.Excercises5.Ex3;

public class Countries extends CountryInfo {

    public Countries(String country, String capital, String primeMinister, String[] languages) {
        super(country, capital, primeMinister, languages);

    }

    public static void main(String[] args) {

        Countries country1 = new Countries("Estonia", "Tallinn", "Jüri Ratas", new String[]{"Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"});
        Countries country2 = new Countries("Latvia", "Riga", "Māris Kučinskis", new String[]{"Latvian", "Russian", "Ukrainian", "Belarusian"});
        Countries country3 = new Countries("Lithuania", "Vilnius", "Saulius Skvernelis", new String[]{"Lithuanian", "Russian", "Ukrainian", "Belarusian", "Polish"});
        Countries country4 = new Countries("Finland", "Helsinki", "Juha Sipilä", new String[]{"Finnish", "Swedish", "Russian"});
        Countries country5 = new Countries("Sweden", "Stockholm", "Stefan Löfven", new String[]{"Swedish", "Finnish", "Meänkieli", "Sami"});
        Countries country6 = new Countries("Spain", "Madrid", "Mariano Rajoy", new String[]{"Spanish", "Basque", "Catalan"});
        Countries country7 = new Countries("Croatia", "Zagreb", "Andrej Plenković", new String[]{"Croatian", "Czech", "Hungarian", "Italian"});
        Countries country8 = new Countries("Denmark", "Copenhagen", "Lars Løkke Rasmussen", new String[]{"Danish", "Faroese", "German", "Swedish"});
        Countries country9 = new Countries("France", "Paris", "Édouard Philippe", new String[]{"French", "English", "Spanish", "German"});
        Countries country10 = new Countries("Norway", "Oslo", "Erna Solberg", new String[]{"Norwegian", "Sami", "English"});


        System.out.println(country1);
        System.out.println(country2);
        System.out.println(country3);
        System.out.println(country4);
        System.out.println(country5);
        System.out.println(country6);
        System.out.println(country7);
        System.out.println(country8);
        System.out.println(country9);
        System.out.println(country10);
    }
}




